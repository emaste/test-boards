# Test Boards

This repository contains small test boards.  These may be footprint tests,
small subcircuits, etc.

## P-channel MOSFET load switch

A SOT-23 GSD P-channel MOSFET with gate RC network to slow transition and avoid
excessive inrush on the controlled load.  See [ON Semiconductor app note AND9093/D](https://www.onsemi.com/pub/Collateral/AND9093-D.PDF)
for details.

PCB at [OSHPark](https://oshpark.com/shared_projects/oig1C1vy).
